//
// Created by sffc on 9/15/17.
//

#ifndef NUMBERFORMAT_TEMP_TEST_HELPERS_H
#define NUMBERFORMAT_TEMP_TEST_HELPERS_H

#include <iostream>
#include <unicode/utypes.h>
#include <unicode/unistr.h>
#include <unicode/ustream.h>

using namespace std;

static void assertSuccess(const char *message, const UErrorCode &status) {
    if (U_FAILURE(status)) {
        cout << "STATUS FAILURE: " << message << ": " << u_errorName(status) << endl;
    }
}

static void assertSuccess(const UnicodeString &message, const UErrorCode &status) {
    if (U_FAILURE(status)) {
        cout << "STATUS FAILURE: " << message << ": " << u_errorName(status) << endl;
    }
}

static void assertEquals(const char *message, const UnicodeString &a, const UnicodeString &b) {
    if (a != b) {
        cout << "FAIL: " << message << ": " << a << " vs " << b << endl;
    } else {
#ifndef TEMP_TEST_HELPERS_NO_PRINT_PASS
        cout << "PASS: " << a << endl;
#endif
    }
}

static void assertEquals(const UnicodeString &message, const UnicodeString &a, const UnicodeString &b) {
    if (a != b) {
        cout << "FAIL: " << message << ": " << a << " vs " << b << endl;
    } else {
#ifndef TEMP_TEST_HELPERS_NO_PRINT_PASS
        cout << "PASS: " << a << endl;
#endif
    }
}

static void assertEquals(const char *message, const UErrorCode& a, const UErrorCode& b) {
    if (a != b) {
        cout << "FAIL: " << message << ": " << u_errorName(a) << " vs " << u_errorName(b) << endl;
    } else {
#ifndef TEMP_TEST_HELPERS_NO_PRINT_PASS
        cout << "PASS: " << a << endl;
#endif
    }
}

static void assertEquals(const UnicodeString &message, const UErrorCode& a, const UErrorCode& b) {
    if (a != b) {
        cout << "FAIL: " << message << ": " << u_errorName(a) << " vs " << u_errorName(b) << endl;
    } else {
#ifndef TEMP_TEST_HELPERS_NO_PRINT_PASS
        cout << "PASS: " << a << endl;
#endif
    }
}

static void assertTrue(const char *message, bool b) {
    if (!b) {
        cout << "FAIL: " << message << ": Expected true" << endl;
    } else {
        cout << "PASS: " << message << endl;
    }
}

static void assertTrue(const UnicodeString &message, bool b) {
    if (!b) {
        cout << "FAIL: " << message << ": Expected true" << endl;
    } else {
        cout << "PASS: " << message << endl;
    }
}

static void assertFalse(const char *message, bool b) {
    if (b) {
        cout << "FAIL: " << message << ": Expected false" << endl;
    } else {
        cout << "PASS: " << message << endl;
    }
}

static void assertFalse(const UnicodeString &message, bool b) {
    if (b) {
        cout << "FAIL: " << message << ": Expected false" << endl;
    } else {
        cout << "PASS: " << message << endl;
    }
}

static void errln(const UnicodeString &message) {
    cout << "FAIL: " << message << endl;
}

#endif //NUMBERFORMAT_TEMP_TEST_HELPERS_H
