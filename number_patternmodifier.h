//
// Created by sffc on 9/9/17.
//

#ifndef NUMBERFORMAT_MUTABLEPATTERNMODIFIER_H
#define NUMBERFORMAT_MUTABLEPATTERNMODIFIER_H

#include <standardplural.h>
#include "unicode/numberformatter.h"
#include "number_patternstring.h"
#include "number_types.h"
#include "number_modifiers.h"
#include "number_utils.h"

U_NAMESPACE_BEGIN
namespace number {
namespace impl {

class ImmutablePatternModifier : public MicroPropsGenerator {
  public:
    ImmutablePatternModifier(ParameterizedModifier *pm, const PluralRules *rules,
                             const MicroPropsGenerator *parent);

    ~ImmutablePatternModifier() override = default;

    void processQuantity(DecimalQuantity &, MicroProps &micros, UErrorCode &status) const override;

    void applyToMicros(MicroProps &micros, DecimalQuantity &quantity) const;

  private:
    const LocalPointer<ParameterizedModifier> pm;
    const PluralRules *rules;
    const MicroPropsGenerator *parent;
};

/**
 * This class is a {@link Modifier} that wraps a decimal format pattern. It applies the pattern's affixes in
 * {@link Modifier#apply}.
 *
 * <p>
 * In addition to being a Modifier, this class contains the business logic for substituting the correct locale symbols
 * into the affixes of the decimal format pattern.
 *
 * <p>
 * In order to use this class, create a new instance and call the following four setters: {@link #setPatternInfo},
 * {@link #setPatternAttributes}, {@link #setSymbols}, and {@link #setNumberProperties}. After calling these four
 * setters, the instance will be ready for use as a Modifier.
 *
 * <p>
 * This is a MUTABLE, NON-THREAD-SAFE class designed for performance. Do NOT save references to this or attempt to use
 * it from multiple threads! Instead, you can obtain a safe, immutable decimal format pattern modifier by calling
 * {@link MutablePatternModifier#createImmutable}, in effect treating this instance as a builder for the immutable
 * variant.
 */
class MutablePatternModifier
        : public MicroPropsGenerator, public Modifier, public SymbolProvider, public CharSequence {
  public:

    ~MutablePatternModifier() override = default;

    /**
     * @param isStrong
     *            Whether the modifier should be considered strong. For more information, see
     *            {@link Modifier#isStrong()}. Most of the time, decimal format pattern modifiers should be considered
     *            as non-strong.
     */
    explicit MutablePatternModifier(bool isStrong);

    /**
     * Sets a reference to the parsed decimal format pattern, usually obtained from
     * {@link PatternStringParser#parseToPatternInfo(String)}, but any implementation of {@link AffixPatternProvider} is
     * accepted.
     */
    void setPatternInfo(const AffixPatternProvider *patternInfo);

    /**
     * Sets attributes that imply changes to the literal interpretation of the pattern string affixes.
     *
     * @param signDisplay
     *            Whether to force a plus sign on positive numbers.
     * @param perMille
     *            Whether to substitute the percent sign in the pattern with a permille sign.
     */
    void setPatternAttributes(UNumberSignDisplay signDisplay, bool perMille);

    /**
     * Sets locale-specific details that affect the symbols substituted into the pattern string affixes.
     *
     * @param symbols
     *            The desired instance of DecimalFormatSymbols.
     * @param currency
     *            The currency to be used when substituting currency values into the affixes.
     * @param unitWidth
     *            The width used to render currencies.
     * @param rules
     *            Required if the triple currency sign, "¤¤¤", appears in the pattern, which can be determined from the
     *            convenience method {@link #needsPlurals()}.
     */
    void
    setSymbols(const DecimalFormatSymbols *symbols, const CurrencyUnit &currency, UNumberUnitWidth unitWidth,
               const PluralRules *rules);

    /**
     * Sets attributes of the current number being processed.
     *
     * @param isNegative
     *            Whether the number is negative.
     * @param plural
     *            The plural form of the number, required only if the pattern contains the triple currency sign, "¤¤¤"
     *            (and as indicated by {@link #needsPlurals()}).
     */
    void setNumberProperties(bool isNegative, StandardPlural::Form plural);

    /**
     * Returns true if the pattern represented by this MurkyModifier requires a plural keyword in order to localize.
     * This is currently true only if there is a currency long name placeholder in the pattern ("¤¤¤").
     */
    bool needsPlurals() const;

    /**
     * Creates a new quantity-dependent Modifier that behaves the same as the current instance, but which is immutable
     * and can be saved for future use. The number properties in the current instance are mutated; all other properties
     * are left untouched.
     *
     * <p>
     * The resulting modifier cannot be used in a QuantityChain.
     *
     * <p>
     * CREATES A NEW HEAP OBJECT; THE CALLER GETS OWNERSHIP.
     *
     * @return An immutable that supports both positive and negative numbers.
     */
    ImmutablePatternModifier *createImmutable(UErrorCode &status);

    /**
     * Creates a new quantity-dependent Modifier that behaves the same as the current instance, but which is immutable
     * and can be saved for future use. The number properties in the current instance are mutated; all other properties
     * are left untouched.
     *
     * <p>
     * CREATES A NEW HEAP OBJECT; THE CALLER GETS OWNERSHIP.
     *
     * @param parent
     *            The QuantityChain to which to chain this immutable.
     * @return An immutable that supports both positive and negative numbers.
     */
    ImmutablePatternModifier *
    createImmutableAndChain(const MicroPropsGenerator *parent, UErrorCode &status);

    MicroPropsGenerator &addToChain(const MicroPropsGenerator *parent);

    void processQuantity(DecimalQuantity &, MicroProps &micros, UErrorCode &status) const override;

    int32_t apply(NumberStringBuilder &output, int32_t leftIndex, int32_t rightIndex,
                  UErrorCode &status) const override;

    int32_t getPrefixLength(UErrorCode &status) const override;

    int32_t getCodePointCount(UErrorCode &status) const override;

    bool isStrong() const override;

    /**
     * Returns the string that substitutes a given symbol type in a pattern.
     */
    UnicodeString getSymbol(AffixPatternType type) const override;

    int32_t length() const override;

    char16_t charAt(int32_t index) const override;

    // Use default implementation of codePointAt

    UnicodeString toUnicodeString() const override;

  private:
    // Modifier details
    const bool fStrong;

    // Pattern details
    const AffixPatternProvider *patternInfo;
    UNumberSignDisplay signDisplay;
    bool perMilleReplacesPercent;

    // Symbol details
    const DecimalFormatSymbols *symbols;
    UNumberUnitWidth unitWidth;
    char16_t currencyCode[4];
    const PluralRules *rules;

    // Number details
    bool isNegative;
    StandardPlural::Form plural;

    // QuantityChain details
    const MicroPropsGenerator *parent;

    // Transient CharSequence fields
    bool inCharSequenceMode;
    int32_t fFlags;
    int32_t fLength;
    bool prependSign;
    bool plusReplacesMinusSign;

    /**
     * Uses the current properties to create a single {@link ConstantMultiFieldModifier} with currency spacing support
     * if required.
     *
     * <p>
     * CREATES A NEW HEAP OBJECT; THE CALLER GETS OWNERSHIP.
     *
     * @param a
     *            A working NumberStringBuilder object; passed from the outside to prevent the need to create many new
     *            instances if this method is called in a loop.
     * @param b
     *            Another working NumberStringBuilder object.
     * @return The constant modifier object.
     */
    ConstantMultiFieldModifier *createConstantModifier(UErrorCode &status);

    int32_t insertPrefix(NumberStringBuilder &sb, int position, UErrorCode &status);

    int32_t insertSuffix(NumberStringBuilder &sb, int position, UErrorCode &status);

    void enterCharSequenceMode(bool isPrefix);

    void exitCharSequenceMode();
};


}  // namespace impl
}  // namespace number
U_NAMESPACE_END

#endif //NUMBERFORMAT_MUTABLEPATTERNMODIFIER_H
