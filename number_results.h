//
// Created by Shane Carr on 9/11/17.
//

#ifndef NUMBERFORMAT_NUMFMTTER_RESULTS_H
#define NUMBERFORMAT_NUMFMTTER_RESULTS_H

#include "number_types.h"
#include "number_decimalquantity.h"
#include "number_stringbuilder.h"

U_NAMESPACE_BEGIN namespace number {
namespace impl {

} // namespace impl
} // namespace number
U_NAMESPACE_END

#endif //NUMBERFORMAT_NUMFMTTER_RESULTS_H
