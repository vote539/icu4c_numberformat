#include <iostream>
#include <unicode/locid.h>
#include <unicode/ustream.h>
#include <unicode/nounit.h>
#include <unicode/measunit.h>
#include <cmath>
#include <cmemory.h>
#include <unicode/uclean.h>
#include "unicode/numberformatter.h"
#include "number_decimalquantity.h"
#include "number_types.h"
#include "number_decimfmtprops.h"
#include "number_patternstring.h"
#include "number_patternmodifier.h"
#include "temp_test_helpers.h"

using namespace std;
using namespace icu::number;
using namespace icu::number::impl;

int main() {
    Locale loc("en");
    std::cout << loc.getName() << std::endl;

    UErrorCode status = U_ZERO_ERROR;
    LocalizedNumberFormatter nf = NumberFormatter::with()
            .notation(Notation::compactShort())
            .unit(NoUnit::percent())
            .unit(CurrencyUnit(u"USD", status))
            .adoptUnit(MeasureUnit::createAcre(status))
            .unitWidth(UNUM_UNIT_WIDTH_FULL_NAME)
            .locale(loc)
            .decimal(UNUM_DECIMAL_SEPARATOR_ALWAYS);

    UnicodeString result = nf.formatInt(-9990, status).toString();
    cout << result << endl;
    result = nf.formatInt(0, status).toString();
    cout << result << endl;
    result = nf.formatInt(51423, status).toString();
    cout << result << endl;

    u_cleanup();

    return 0;
}